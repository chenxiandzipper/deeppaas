# deeppaas

#### 介绍
Deeppaas无代码开发平台

#### 软件架构
软件架构说明

### 安装教程

下载发行版最新版本zip文件解压到您本地并解压；
在您的计算机上安装JavaJDK17环境；jdk推荐下载 https://www.azul.com/downloads/?version=java-17-lts&package=jdk#zulu
安装MySQL5.7以及以上版本，建议8；（mysql推荐下载https://dev.mysql.com/downloads/mysql/）

新建mysql数据库并创建库名为deeppaas的数据库，字符集utf8mb4，排序规则utf8mb4_general_ci。

 **初始化数据库链接** 

修改数据库连接就是修改config文件夹内的application.properties
#连接地址
spring.datasource.jdbc-url=jdbc:mysql://localhost:3306/deeppaas?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true
#数据库用户名
spring.datasource.username=root
#数据库密码
spring.datasource.password=123456

数据库名如果不是deeppaas，那么需要修改默认schema与数据库同名
dp.datasource.default-schema=deeppaas

### 启动说明

windows用户直接运行start.bat启动
linux用户可cd到安装文件夹执行:sh deeppaas.sh start启动

### license申请

如需license请访问[license申请](http://v3.deeppaas.com/apply)进行申请

deeppaas v3.0.4版
开发人员正在开发的大功能：
1、流程引擎与页面的关系等
2、角色权限增加权限分组、增加业务主键
3、其他操作上的功能优化